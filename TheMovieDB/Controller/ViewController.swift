//
//  ViewController.swift
//  TheMovieDB
//
//  Created by Тимур Чеберда on 22.06.2019.
//  Copyright © 2019 Tmur Cheberda. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let cellId = "cellID"
    let tableView = UITableView()
    let array: [MovieTableViewCell.ViewModel] = [
                                        MovieTableViewCell.ViewModel(name: "Sasha", date: "12"),
                                        MovieTableViewCell.ViewModel(name: "Nastya", date: "21")]

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Hello"
        view.backgroundColor = .white
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(MovieTableViewCell.self, forCellReuseIdentifier: cellId)
        view.addSubview(tableView)
        setupTableConstraints()
    }
    
    private func setupTableConstraints() {
        tableView.snp.makeConstraints { (make) in
            make.trailing.equalTo(view.snp.trailing)
            make.top.equalTo(view.snp.top)
            make.leading.equalTo(view.snp.leading)
            make.bottom.equalTo(view.snp.bottom)
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MovieTableViewCell
        cell.setupViewModel(viewModel: array[indexPath.row])
        return cell
    }    
}
