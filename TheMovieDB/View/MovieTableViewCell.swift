//
//  MovieTableViewCell.swift
//  TheMovieDB
//
//  Created by Тимур Чеберда on 22.06.2019.
//  Copyright © 2019 Tmur Cheberda. All rights reserved.
//

import UIKit
import SnapKit

class MovieTableViewCell: UITableViewCell {
    let movieName = UILabel()
    let movieDate = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(movieName)
        contentView.addSubview(movieDate)
        
        setupLabel()
        setuConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLabel() {
        movieName.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        movieName.numberOfLines = 0
        
        movieDate.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        movieDate.numberOfLines = 0
    }
    
    private func setuConstraints() {
        movieName.snp.makeConstraints { (make) in
            make.top.equalTo(contentView).offset(16)
            make.left.equalTo(contentView).offset(12)
            make.right.equalTo(contentView).offset(-12)
            make.bottom.equalTo(movieDate.snp.top).offset(-10)
        }
        
        movieDate.snp.makeConstraints { (make) in
            make.top.equalTo(movieName.snp.bottom).offset(10)
            make.left.equalTo(contentView).offset(12)
            make.right.equalTo(contentView).offset(-12)
            make.bottom.equalTo(contentView).offset(-16)
        }
    }
}

extension MovieTableViewCell {
    struct ViewModel {
        let name: String
        let date: String
    }
    
    func setupViewModel(viewModel: ViewModel) {
        movieName.text = viewModel.name
        movieDate.text = viewModel.date
    }
}
